/*
 ============================================================================
 Name        : main.c
 Authors     : Rakesh Kaim, Gurdeep
 Version     : 0.0.1
 License     : GNU General Public License v3.0
 Description : Non Assertive test functions for API sort.h 
 ============================================================================
 */

#include <sort.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

/*
 *  For collecting performance and statistical data
 *  comps - represents the number of comparisons
 *  swaps - represents the number of swaps
 *  wr    - represents the number of writes
 */
int comps, swaps, wr;

/**
 * An enum type represents the size of a given array.
 */
enum size {
	SIZE = 10
};

/**
 * An enum type represents the specific test scenario.
 * BEST   a fully sorted array scenario.
 * WORST  a fully reverse sorted array scenario.
 * AVERAGE  a partially sorted array scenario.
 * RANDOM  a randomly filled array scenario.
 */
enum testScenario {
	BEST, AVERAGE, WORST, RANDOM
};

/**
 * An enum type alias
 * @param analysis the type of test scenario
 */

typedef enum testScenario analysis;

/**
 * An Integer array to hold the test data.
 */
int testData[SIZE];

/**
 * A function to initialize the test data 
 * depends upon test scenario.
 * @param size the contant size of the input array.
 * @param scenario the test scenario name and 
 * possible values are {WORST, RANDOM, BEST, AVERAGE}
 */
void initTestData(const int size, analysis scenario){
	const int lastIndex = size - 1;
	const int midIndex = size/2;

	if (scenario == WORST)
		for (int i = lastIndex; i >= 0; --i)
			testData[lastIndex - i] = i;

	else if (scenario == RANDOM)
		for (int i = 0; i < size; i++)
			testData[i] = (rand() % size);

	else if (scenario == BEST)
		for (int i = 0; i < size; i++)
			testData[i] = i;

	else if (scenario == AVERAGE){
			for (int i = 0; i < size; i++){
				if(i < midIndex)
					testData[i] = i;
				else
					testData[i] = rand() % size;
			}
	}

	//_print_(testData, size, "\tmain.c:initTestData()", 0);
}

/**
 * Non Assertive test for Bubble Sort Algorithm.
 */
void testBubbleSort() {
	bubbleSort(testData, SIZE);
}

/**
 * Non Assertive test for Selection Sort Algorithm.
 */
void testSelectionSort(){
	selectionSort(testData, SIZE);
}

/**
 * Non Assertive test for Insertion Sort Algorithm.
 */
void testInsertionSort(){
	insertionSort(testData, SIZE);
}

/**
 * Non Assertive test for Quick Sort Algorithm.
 */
void testQuickSort(){
  _print_(testData, SIZE, "\tsort.c:quickSort():Pass-", -1);

  int l = 0;
	int h = SIZE-1;
	quickSort(testData, l, h, SIZE);
  _print_(testData, SIZE, "\tsort.c:quickSort():Pass-", -1);
  printf("\nCompsparisons: %d,\tSwaps: %d\n\n", comps, swaps);
}

/**
 * Non Assertive test for Merge Sort Algorithm.
 */
void testMergeSort(){
  _print_(testData, SIZE, "\tsort.c:mergeSort():Pass-", -1);

  int i = 0;
	int j = SIZE-1;
	mergeSort(testData, i, j);
  _print_(testData, SIZE, "\tsort.c:mergeSort():Pass-", -1);
  printf("\nCompsparisons: %d,\twrites: %d\n\n", comps, wr);
}



/**
 * Main function
 * @param args the test scenario to execute.
 * for eg. main {best, worst, random, average}
 */
int main(int argc, char *args[]) {

	analysis testScenario;
	char *headerMsg;

	if(argc > 1){
		if (strcmp(args[1],"random") == 0){
			testScenario = RANDOM;
			headerMsg = "Test Scenario: RANDOM Case";
		}
		else if (strcmp(args[1],"worst") == 0){
			testScenario = WORST;
			headerMsg = "Test Scenario: WORST Case";
		}
		else if (strcmp(args[1],"best") == 0){
			testScenario = BEST;
			headerMsg = "Test Scenario: BEST Case";
		}
		else if (strcmp(args[1],"average") == 0){
			testScenario = AVERAGE;
			headerMsg = "Test Scenario: AVERAGE Case";
		}
		else {
			testScenario = BEST;
			headerMsg = "Test Scenario: BEST Case";
		}
	} else {
		testScenario = BEST;
		headerMsg = "Test Scenario: BEST Case";
	}

	printf("%s\n", headerMsg);

  initTestData(SIZE, testScenario);
  testBubbleSort();
  initTestData(SIZE, testScenario);
  testSelectionSort();
  initTestData(SIZE, testScenario);
  testInsertionSort();
  initTestData(SIZE, testScenario);
  testQuickSort();
  initTestData(SIZE, testScenario);
  testMergeSort();
  return 0;
}
