#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct node {
  int data;
  struct node *next;
  struct node *prev;
}node;

node *head, *tail;


node *create(int data){
  node * a = (node *)malloc(sizeof(node));
  if(a == NULL){
    printf("\n error \n");
    exit(0);
  }
    a->data = data;
    a->next = NULL;
    a->prev = head;
    return a;
}

node *append(int data){

  if(head == NULL)
    return create(data);

  node *p = head;

  while(p->next != NULL)
    p = p->next;

  p->next = create(data);
  return head;
}
