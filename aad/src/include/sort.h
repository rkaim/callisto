/*
 ============================================================================
 Name        : sort.h
 Authors     : Rakesh Kaim, Gurdeep
 Version     : 0.0.1
 License     : GNU General Public License v3.0
 Description : Collection of all the standard sorting algorithms.
 ============================================================================
 */

#ifndef SORT_H
#define SORT_H
#endif

/**
 * A utility function to swap [i] th and [j] th element of an array.
 * @param i the [i] th element of given input array.
 * @param j the [j] th element of given input array.
 */
extern void swap(int *i, int *j);

/**
 * A utility function to print an array with a given size and 
 * with a breif info log.
 * @param *p the integer pointer to the 1st element of given array.
 * @param size the contant size of given input array.
 * @param *msg the info log message to be printed.
 * @param pass the integer value to repsents the number of sorting passes.
 */
extern void _print_(int *p, const int size, char *msg, int pass);

/**
 * A function to sort a given array using Bubble Sort Algorithm.
 * @param *a the integer pointer to the 1st element of given array.
 * @param size the constant integer value to represents the size of.
 * a given input array
 */
extern void bubbleSort(int *a, const int size);

/**
 * A function to sort a given array using Slection Sort Algorithm.
 * @param *a the integer pointer to the 1st element of given array.
 * @param size the constant integer value to represents the size of
 * a given input array.
 */
extern void selectionSort(int *a, const int size);

/**
 * A function to sort a given array using Insertion Sort Algorithm.
 * @param *a the integer pointer to the 1st element of given array.
 * @param size the constant integer value to represents the size of
 * a given input array.
 */
extern void insertionSort(int *a, const int size);

/**
 * A function to sort a given array using Quick Sort Algorithm
 * @param *a is the integer pointer to the 1st element of given array
 * @param l is the starting index of the given array
 * @param h is the ending index of the given array
 * @param size the constant integer value to represents the size of
 * a given input array.
 */
extern void quickSort(int *a, int l, int h, int size);

/**
 * A function to sort a given array using Merge Sort Algorithm.
 * @param *a is the integer pointer to the 1st element of given array
 * @param i is the starting index of the given array
 * @param j is the ending index of the given array
 */
extern void mergeSort(int *a, int i, int j);
