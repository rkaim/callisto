/*
  ============================================================================
  Name        : Queue.c
  Authors     : Rakesh Kaim, Gurdeep
  Version     : 0.0.1
  License     : GNU General Public License v3.0
  Description : Implementation Of Queue Data Structure
  ============================================================================
*/
# include <stdio.h>
# include <stdlib.h>

 typedef struct node{
  int data;
  struct node *next;
 }node;

struct node *head = NULL, *tail = NULL, *temp;

/**
 * A utility function to add a new node in the link list using Queue data stucture. 
 */
void push(int data){
  node * a = (node *)malloc(sizeof(node));
  if(a == NULL){
    printf("\n error \n");
    exit(0);
  }
  a->data = data;
  a->next = NULL;
  if(head == NULL && tail == NULL)
    head = tail = a;
  else{
    tail->next = a;
    tail = a;
  }
}

/**
 * A utility function to remove a existing node from the link list using Queue data stucture. 
 */
void Remove(){
  if(head == NULL){
    printf("\n Queue is Empty \n");
    exit (0);
  }
  else{
    temp = head;
    head = head->next;
    free(temp);
  }
}

/**
 * A utility function to remove a existing node from the link list using Queue data stucture. 
 */
void printlist(){

  node *p = head;
  printf("\n [");
  while(p != NULL){
    printf(" %d, ", p->data);
    p = p->next;
  }
  printf("] \n");
}
