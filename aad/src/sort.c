/*
 ============================================================================
 Name        : sort.c
 Authors     : Rakesh Kaim, Gurdeep
 Version     : 0.0.1
 License     : GNU General Public License v3.0
 Description : Collection of all the standard sorting algorithms.
 ============================================================================
 */
#include <stdio.h>
#include <stdbool.h>

/**
 * A utility function to swap [i] th and [j] th element of an array.
 * @param i the [i] th element of given input array.
 * @param j the [j] th element of given input array.
 */
void swap(int *i, int *j) {
	int t = *i;
	*i = *j;
	*j = t;
	return;
}

/**
 * A utility function to print an array with a given size and 
 * with breif info log.
 * @param *p the integer pointer to the 1st element of given array.
 * @param size the contant size of given input array.
 * @param *msg the info log message to be printed.
 * @param pass the integer value to repsents the number of sorting passes. 
 */
void _print_(int *p, const int size, char *msg, int pass){

	printf("[");
	for (int i = 0; i < size; ++i){
		if (i < size - 1)
			printf("%d, ", p[i]);
		else
			printf("%d", p[i]);
  }
  if(pass == -1)
		printf("]%s\n", msg);
	else
		printf("]%s%d\n", msg, pass);
	return;
}

/**
 * A function to sort a given array using Bubble Sort Algorithm.
 * @param *a the integer pointer to the 1st element of given array.
 * @param size the constant integer value to represents the size of 
 * a given input array.
 */
void bubbleSort(int *a, const int size){
	/*
	 *  For collecting performance statistical data
	 *	com - number of comparisons
	 *  swp - number of swaps
	 */
	int com = 0, swp = 0, i = 0;
	bool isSwapped = false;
	for (; i < size - 1; ++i){
    _print_(a, size, "\tsort.c:bubbleSort():Pass-", i);

		for (int j = size - 1; j >= i + 1; --j){
			++com;
			if (a[j] < a[j - 1]){
				isSwapped = true;
				++swp;
				swap(&a[j], &a[j - 1]);
			}
		}
		if (!isSwapped)
			break;
	}
	_print_(a, size, "\tsort.c:bubbleSort():Pass-", i);

	/*for (int i = 0; i < size - 1; i++) {
	 for (int j = 0; j < size - i - 1; j++){
	 com++;
	 if (a[j] > a[j + 1]) {
	 isSwapped = true;
	 sw++; swap(&a[j], &a[j +1]);
	 }
	 }
	 _print_(a,size);
	 if(!isSwapped)
	 break;
	 }*/
	printf("\nComparisons: %d,\tSwaps: %d\n\n", com, swp);
	return;
}

/**
 * A function to sort a given array using Slection Sort Algorithm
 * @param *a the integer pointer to the 1st element of given array.
 * @param size the constant integer value to represents the size of
 * a given input array.
 */
void selectionSort(int *a, int size){

  	/*
	 *  For collecting performance and statistical data
	 *	com - represents the number of comparisons
	 *  sw - represents the number of swaps
	 */
	int com = 0, swp = 0, i, j;
    for(i = 0; i < size - 1; i++){
      _print_(a, size, "\tsort.c:selectionSort():Pass-", i);
      for(j = i+1; j < size ; j++){
          ++com;
          if(a[j] < a[i]){
            ++swp;
            swap(&a[i], &a[j]);
          }
      }
    }

    _print_(a, size, "\tsort.c:selectionSort():Pass-", i);
    printf("\nComparisons: %d,\tSwaps: %d\n\n", com, swp);

    return;
}
/**
 * A function to sort a given array using Insertion Sort Algorithm.
 * @param *a the integer pointer to the 1st element of given array.
 * @param size the constant integer value to represents the size of 
 * a given input array.
 */
void insertionSort(int *a, int size){

  /*
	 *  For collecting performance and statistical data
	 *	com - represents the number of comparisons
	 *  wr - represents the number of writes
	 */

  int com =0, wr = 0, i, j, temp;
  for (i = 1; i < size; i++){
    _print_(a, size, "\tsort.c:insertionSort():Pass-", i);
    temp = a[i];
    j = i - 1;
    ++com;
    while (j >= 0 && a[j] > temp){
      ++com;
      a[j + 1] = a[j];
      j = j - 1;
    }
    a[j + 1] = temp;
    ++wr;
  }
  _print_(a, size, "\tsort.c:insertionSort():Pass-", i);
  printf("\nComparisons: %d,\tWrite: %d\n\n", com, wr);
}

/**
 utility function to help quickSort.
 This function takes last element as pivot, places
 the pivot element at its correct position in sorted
 array, and places all smaller (smaller than pivot)
 to left of pivot and all greater elements to right
 of pivot
*/

int partition2(int *a, int l, int h){
  /*
	 *  For collecting performance statistical data
	 *	com - number of comparisons
	 *  swp - number of swaps
	 */
  extern int comps, swaps;

  int i = l-1, j = h;
  int pivot = a[h];
  for(;;){
    while (a[++i] < pivot)
      ++comps;
    while (pivot < a[--j]){
      ++comps;
      if (j == l)break;
    }
    if (i >= j)break;
    swap(&a[i], &a[j]);
    ++swaps;
  }
  if(i != h){
    swap(&a[i], &a[h]);
    ++swaps;
  }
  return i;
}

/**
 * A function to sort a given array using Quick Sort Algorithm
 * @param *a the integer pointer to the 1st element of given array.
 * @param size the constant integer value to represents the size of
 * a given input array.
 */
void quickSort(int *a, int l, int h, int size){
  int j = 0;
  if(h <= l)
    return;
  j = partition2(a, l, h);
  quickSort(a, l, j-1, size);
  quickSort(a, j+1, h, size);
}


/**
   utility function to help merge sort.
*/
void merge(int *arr, int a, int b, int c, int d , int size){
  /*
	 *  For collecting performance statistical data
	 *	com - number of comparisons
	 *  wr - number of writes
	 */
  extern int comps, wr;
  size = d;
  int t[size + 1];
  int i = a, j = c, k = 0;

  while (i <= b && j <= d){
    if (arr[i] < arr[j]){
      ++comps;
      t[k++] = arr[i++];
      ++wr;
    }
    else{
      t[k++] = arr[j++];
      ++wr;
    }
    ++comps;
  }
  while (i <= b){
    ++comps;
    t[k++] = arr[i++];
    ++wr;
  }

  while (j <= d){
    ++comps;
    t[k++] = arr[j++];
    ++wr;
  }

  for (i = a, j = 0; i <= d; i++, j++){
    arr[i] = t[j];
    ++wr;
  }

}
/**
 * A function to sort a given array using Merge Sort Algorithm.
 * @param *a the integer pointer to the 1st element of given array
 * @param i is the constant integer value to represents the starting index of
 * a given input array
 ^ @param j is the constant integer value to represents the ending index of
 * a given input array.
 */

void mergeSort(int *a, int i, int j){
  int m;

  if (i < j){
    m = (i + j) / 2;
    mergeSort(a, i, m);
    mergeSort(a, m + 1, j);
    merge(a, i, m, m + 1, j, j);
  }
}
